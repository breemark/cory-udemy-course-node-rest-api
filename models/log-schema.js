const db = require('../config/db');

const LogSchema = db.Schema({
    description: String,
    result: String,
    date: { type: Date, default: Date.now },
    owner: { Type: db.Schema.Types.ObjectId, ref: 'User'},
    definition: { Type: db.Schema.Types.ObjectId, ref: 'Definition'}
});

module.exports = LogSchema;
