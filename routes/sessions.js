const router = require('express').Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const constants = require('../config/constants');
const User = require('../models/user');

router.post('/', (req, res) => {
    User.findOne({ username: req.body.user.username}).then(
        (user) => {
            if (user) {
                bcrypt.compare(req.body.user.pwd, user.passhash, (err, matches) => {
                    if (matches) {
                        const sessionToken = jwt.sign(user._id, constants.JWT_SECRET, { expiresIn: 24*60*60 });
                        res.json({
                            user: user,
                            message: 'succesfully authed',
                            sessionToken: sessionToken
                        })
                    } else {
                        res.json({
                            user: {},
                            message: 'failed to auth',
                            sessionToken: ''
                        });
                    }
                });
            } else {
                res.json({
                    user: {},
                    message: 'failed to auth',
                    sessionToken: ''
                });
            }
        },
        (err) => {
            // Could not find User
            res.json(err);
        }
    );
});

module.exports = router;