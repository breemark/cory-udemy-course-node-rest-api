const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.json());

app.use(require('./middleware/headers'));
app.use(require('./middleware/validate-session'));

app.use('/test', (req, res) => {
    res.send('hola world');
});

app.use('/api/users', require('./routes/users'));
app.use('/api/login', require('./routes/sessions'));
app.use('/api/definitions', require('./routes/definitions'));


app.listen(3000, () => {
    console.log('app listening on port 3000');
});